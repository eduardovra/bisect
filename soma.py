#!/usr/bin/python

# Commit 9

import sys

def converte_lista_para_numeros (lista_str):
    return list(map(int, lista_str))

def soma_lista_numeros (lista_numeros):
    return sum(lista_numeros)

def main ():
    argumentos = sys.argv[1:]
    lista_numeros = converte_lista_para_numeros(argumentos)
    print(soma_lista_numeros(lista_numeros))

if __name__ == "__main__":
    main()
